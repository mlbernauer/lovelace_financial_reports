# Lovelace Healthcare System Financial Reports
[Michael L. Bernauer](http://mlbernauer.com)

## Directory Layout and File Descriptions
This repository contains scripts for producing the Lovelace Healthcare System 
Financial Reports. The directory structure is given below.

```
├── config.yml
├── data
│   └── README.md
├── generate_report
├── img
│   └── graphic.jpg
├── lib
│   ├── financial_report.Rmd
│   └── plotlib2.R
├── README.md
└── report.Rmd
```
The `config.yml` file is the configuration file used to customize reports. The `data`
directory contains the Cardinal purchase report (`YYYY_cardinal_report.csv`) as well
as the facility stats spreadsheet (`facility_stats.csvs`). These files can be moved
anywhere on the compute however their location must be updated in the `config.yml`
file under `cardinal_report` and `facility_report`. The `generate_report` file is executable and
is used to generate the financial report. This script works by pulling the most recent
`financial_report.Rmd` files which is a markdown document specifying the layout of the
final report; as a result this script requires an internet connection to work.

## Gernerating Reports
1. Download the most current cardinal report for the time period of interest and save it to the `data` directory. Make sure you update the `cardinal_report` in `config.yml` with the the correct file name and location (e.g. './data/2016_cardinal_report.csv')
2. Update the `facility_stats.csv` spreadsheet to contain the most recent facility metrics.
3. Run the `generate_report` script by double-clicking

## Data Format

### `facility_stats.csv`
The `facility_stats.csv` file contains various facility measures such as the Adjusted
Patient Days (APD), Average Length of Stay (ALOS), number of baby deliveries (Baby.Deliveries), etc.
aggregated by month. It is very important that the facility names listed under `Facility` as spelled
exactly as they appear in the Cardinal Report. Additionally, the dates must be formatted exactly as
they appear below. For example if we are entering data in for December 2016 we must use the following date
format '12/01/2016'. We simply use the first day of the month as a place holder. You can name this file
whatever you want (i.e. 'facility_report_measures.csv') however the new name should be updated in the `config.yml`
file under `facility_report`.

|   |Facility                     |Date     |  APD|  ALOS| Baby.Deliveries| Billed.Doses.Dispensed|  CMI| CVOR.Surgeries| Eye.Surgeries| Patient.Days| Total.Inpatient.Revenue| Total.Outpatient.Revenue| Total.Surgeries| Noncardinal.Drug.Spend|
|:--|:----------------------------|:--------|----:|-----:|---------------:|----------------------:|----:|--------------:|-------------:|------------:|-----------------------:|------------------------:|---------------:|----------------------:|
|75 |Lovelace Women'S Hospital    |9/1/2015 | 4442|  3.77|             228|                  55983| 0.94|             NA|            NA|         2001|                 1526116|                   630083|             466|                  17.14|
|67 |Lovelace Women'S Hospital    |1/1/2015 | 4844|  4.03|             272|                  62847| 0.92|             NA|            NA|         2561|                 1728784|                   612101|             461|               34515.36|
|81 |Lovelace Medical Center      |2/1/2016 | 8115|  5.08|              NA|                 217047| 2.08|             83|           180|         4627|                 5905005|                  1139460|             592|              170080.43|
|59 |Lovelace Westside Hospital   |5/1/2015 | 1811|  3.16|              26|                  24955| 1.18|             NA|            NA|          645|                  579590|                   258911|             234|                  -1.27|
|88 |Lovelace Rehabilitation Hosp |3/1/2016 | 1582| 13.43|              NA|                  33818| 1.25|             NA|            NA|         1222|                  424274|                     5216|              NA|                     NA|

### `YYYY_cardinal_report.csv`
The `YYYY_cardinal_report.csv` file is obtained form the Cardinal website and contains purchase summaries
for a specified period of time aggregated by month. In general, this file should not be edited however,
you may want to open it up to make sure that the dates are formatted properly. Unlike the `facility_stats.csv`
file, this dates in the `YYYY_cardinal_report.csv` file should be formatted as described below. For example,
if we are looking at an entry for December 2016 the correct format would be '16-Dec'. You can name this file
whatever you want (i.e. '2015_cardinal_report.csv') however this should be updated in the `config.yml` file
under `cardinal_report`.

|      |Account.Name               |AHFS.Description                   |Generic.Name    |Trade.Name          |         NDC|Strength  |Form |Size    |Manufacturer              |Unit.Dose |Month.Year | Package.Qty| Ship.Qty|Purchase.Dollars | Package.Size|
|:-----|:--------------------------|:----------------------------------|:---------------|:-------------------|-----------:|:---------|:----|:-------|:-------------------------|:---------|:----------|-----------:|--------:|:----------------|------------:|
|9405  |Lovelace Westside Hospital |Antiarrhythmics, Miscellaneous     |AMIODARONE HCL  |Amiodarone Hcl      | 51079090620|200mg     |TABS |10X10EA |MYLAN INSTITUTIONAL INC   |UD        |15-Jan     |          10|        1|$17.90           |           10|
|59202 |Lovelace Medical Center    |Direct Vasodilators                |HYDRALAZINE HCL |Hydralazine Hcl     | 62584073301|25mg      |TABS |10X10EA |AMERICAN HEALTH PACKAGING |UD        |14-Jul     |          10|        3|$64.05           |           10|
|13342 |Lovelace Westside Hospital |Direct Vasodilators                |HYDRALAZINE HCL |Hydralazine Hcl     | 51079007520|25mg      |TABS |10X10EA |MYLAN INSTITUTIONAL INC   |UD        |15-Mar     |          10|        3|$42.27           |           10|
|61639 |Lovelace Medical Center    |Iron Preparations                  |FE SULFATE      |Ferrous Sulfate Inp |   121053005|300mg/5ml |LIQD |100X5ML |PHARMACEUTICAL ASSOC (CS) |UD        |14-May     |         100|        1|$40.43           |            5|
|13991 |Lovelace Westside Hospital |General Anesthetics, Miscellaneous |PROPOFOL        |Diprivan            | 63323026929|10mg/Ml   |SDPF |10X20ML |FRESENIUS KABI USA LLC    |          |13-Dec     |          10|        1|$21.75           |           20|

## Output
The `generate_report` script will output a pdf document which contains the report.
This pdf is named as follows `yyyymmddHHMM_financial_report.pdf` where `yyyy`
coresponds to the year, `mm` is the month, `dd` is the day, `HH` is the hour and
`MM` is the minute the report was created. The `generate_report` script also creates
several temporary files such as `report.Rmd` which should be automatically removed once
the script is finshed, however if they remain after the script is done they can be
deleted.

## Report Layout
The report is arranged according to facility. Within each facility section subsections
related to various measures. An example is as follows

# Lovelace Health System
This section contains information related to Lovelace Health System

## Facility Statistics
This section contains facility statistics contained in the `facility_stats.csv`.
Statistics include:

1. Date - month for which the statistic was reported
2. APD - adjusted patient days
3. ALOS - average length of stay
4. Baby Delivieries - total number of baby deliveries for the month
5. Billed Doses Dispensed - total number of billed doses dispensed for the month
6. CMI - case mix index
7. CVOR Surgeries - total number of CVOR surgeries for the month
8. Eye Surgeries - total number of eye surgeries for the month
9. Patient Days - total number of patent days
10. Total Inpatient Revenue - total revenue from inpatient services for the month
11. Total Outpatient Revenue - total revenue from outpatient services for the month
12. Total Surgeries - total number of surgeries for the month
13. Noncardinal Drug Spend - total expenditure for drugs purchased outside of Cardinal

### Plots
#### Lovelace Healthcare System: Stats Trends
This plot describes the facility stats trends for the above metrics. The trend
window/time period is defined in the `config.yml` file as `min_date` and `max_date`

## APD Adjusted Total Drug Spend
This section contains the *APD Adjusted Total Pharmacy Spend* plot which plots
the APD spend for the time period of interest `min_date` and `max_date`. In addition
this plot has a `benchmark` overlay which can be set in the `config.yml` file using
`<facility_name>_benchmark`.

## AHFS Classes (Month of Interest)
This seciton plots the top AHFS classes for the month of interest. The month of
interest can be set in the `config.yml` file usng `month_of_interest`.

## AHFS Classes (Date Range)
This seciton contains a boxplot describing the top AHFS classes aggregated over
the entire date range specified in `config.yml`. The boxplot describes the variability
in monthly purchases for any particular AHFS class.

## Top Generics (Month of Interest)
This plot shows the top generics by APD adjusted spend computed for the month of
interest. These values are computed by summing the total number of dollars spent
on the drug for that month and dividing by the APD for that facility reported on that
month. The percent values describe the percent spent on that drug compared to the
total drug spend for that facility during that month.

## Top Generics (Date Range)
This plot contains boxplots describing the various purchases made for a paricular
agent during the specified date range. The agents are arranged by descendingly by 
drug spend averaged over that time period. Again, APD adjusted spend is computed
by computing the total amount spent on the drug for that facility during the month
divided by the APD reported for that facility for the appropriate month.

## Trade Names (Month of Interest)
This plot describes the top drugs by trade name for the month of interest for a 
particular facility. Values are determined by computing the total dollars spent 
on a particular trade name for the month of interest and dividing this number
by the APD reported for that facility during the month of interest.

## Trade Names (Date Range)
This figure contains boxplots which describe the variability associted with monthly
purchases according to trade name for the specified time period. Trade names are
arrange descendingly by mean APD adjusted montly spend.

## Trending Special Classes
These plots describe the monthly trends for agents specified in the `special_classes`
section of the `config.yml` file. Products can be added/removed from the `special_classes` field in the `config.yml` file, however, make sure to enter the class names exactly
as they appear in the Cardinal report (i.e. `YYYY_cardinal_report.csv`). A table is
provided in this section to give a detailed view of the dollar amounts spent
on each agent for each month. The special classes include `Antimicrobial`, `Anticoagulants`, `Antineoplastics`, `Hematopoietics`, `Antiplatelets`, `Antiasthmatics`, and `Insulins`. The values reported in the plots are computed by cumming the monthly expenditures
for each of the agents listed below the class entry (i.e. summing all agents under `Antiplatelets`). Additional class headings can be added by adhering to the format used in
the `config.yml` file. The class heading can be named anything however it is a good
idea to avoid spaces or other unusual characters. For example you can enter another
class heading such as `BetaBlockers` which contains agents such as `Metoprolol` or `Carvedilol`. It's important to know that the AHFS classes used should match what is found
in the Cardinal report. An example of the format that should be used is given below.

```
  BetaBlockers
    - Metoprolol
    - Carvedilol
```  

## Trending Special AHFS Classes
These plots describe the montly trends for the individual AHFS classes listed
underneat the `special_classes` heading in the `config.yml` file. For example, this
plot will show the monthly spend trend for `Tetracyclines` or `Urinary Anti-Infectives`. Additional agents (i.e. AHFS classes) that are added to the `special_classes` section
in the `config.yml` file will be included in this plot. Again, a table is included
to give actual APD adjusted dollar amounts.

## Trending Generics

## Trade Names

## Product Trends
